"""stakeoptions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views
from stake.views import main_view, signup_view, login_view, logout_view, dashboard_view, buy_stake_view, confirm_buy_stake_view, shares_update, deactivate_share_trade, activate_share_trade, staff_shares_update, raise_dispute_view, dispute_view
from stake.views import confirm_paid_view, approve_paid_view,cancel_trade_view, shares_view, transactions_view, nofifications_view, support_view, users_view, ChartData, deactivate_staff_shares, activate_staff_shares, deactivate_user_shares, activate_user_shares

from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', main_view, name='main-view'),
    path('signup/', signup_view, name='signup-view'),
    path('login/', login_view, name='login-view'),
    path('logout/', logout_view, name='logout-view'),



    path('dashboard/', dashboard_view, name='dashboard-view'),
    path('buy_stake/', buy_stake_view, name='buy-stake-view'),
    path('shares/', shares_view, name='shares-view'),
    path('transactions/', transactions_view, name='transactions-view'),
    path('transactions/<str:slug>/', confirm_buy_stake_view, name='confirm-buy-stake-view'),
    path('transactions/<str:slug>/pay/', confirm_paid_view, name='confirm-paid-view'),
    path('transactions/<str:slug>/approve/', approve_paid_view, name='approve-paid-view'),
    path('transactions/<str:slug>/cancel/', cancel_trade_view, name='cancel-paid-view'),
    path('transactions/dispute/<str:slug>/', raise_dispute_view, name='raise-dispute-view'),
    path('transactions/<str:dis_pute_id>/dispute/', dispute_view, name='dispute-view'),



    path('shares-update/', shares_update, name='shares-update-view'),
    path('staff-shares-update/', staff_shares_update, name='staff-shares-update-view'),
    path('shares-update/deactivate/<str:shared>/', deactivate_share_trade, name='deactivate-share-view'),
    path('shares-update/activate/<str:shared>/', activate_share_trade, name='activate-share-view'),
    path('activate-staff/', deactivate_staff_shares, name='deactivate-staff-shares-view'),
    path('deactivate-staff/', activate_staff_shares, name='activate-staff-shares-view'),
    path('activate-user/', deactivate_user_shares, name='deactivate-user-shares-view'),
    path('deactivate-user/', activate_user_shares, name='activate-user-shares-view'),


    path('notifications/', nofifications_view, name='notification-view'),
    path('support/', support_view, name='support-view'),
    path('users/', users_view, name='users-view'),


    path('api/data', ChartData.as_view(), name='api-data'),


    path('reset_password/', auth_views.PasswordResetView.as_view(template_name = "recoverpw-1.html"), name='reset_password'),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name = "recoverpw_sent.html"), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name = "recoverpw_set.html"), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name = "recoverpw_complete.html"), name='password_reset_complete'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)