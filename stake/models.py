from django.db import models
from django.contrib.auth.models import User
from .utils import generate_code


class Profile(models.Model):
    
    user= models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.IntegerField(unique=True, null=True)



class Shares(models.Model):
    
    P_CHOICES = [
            ('4 days(30%)', '4 days(30%)'),
            ('7 days(50%)', '7 days(50%)'),
    ]

    S_CHOICES = [
            ('maturing', 'maturing'),
            ('matured', 'matured'),
            ('completed', 'completed')
    ]
    

    user= models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.IntegerField(null=False)
    interest_period = models.CharField(max_length=30, choices=P_CHOICES)
    status = models.CharField(max_length=30, choices=S_CHOICES)
    expected_payout =models.IntegerField()
    received_payment = models.IntegerField(default=0)
    date_created = models.DateTimeField(auto_now_add=True)
    maturity_date = models.DateTimeField(null=True, blank=True)
    active_trade = models.BooleanField(default=True)
    

    def __str__(self):
        return '%s, %s, %s, %s, %s, %s, %s' %  (self.user, self.amount, self.interest_period, self.status, self.expected_payout, self.received_payment, self.date_created)



class Transaction(models.Model):

    T_CHOICES = [
            ('pending', 'pending'),
            ('cancelled', 'cancelled'),
            ('paid', 'paid'),
            ('approved', 'approved')
    ]
    
    P_CHOICES = [
            ('4 days(30%)', '4 days(30%)'),
            ('7 days(50%)', '7 days(50%)'),
    ]

    
    trans_id = models.CharField(max_length=12, blank=True, unique=True)
    amount = models.IntegerField(null=False)
    status = models.CharField(max_length=30, choices=T_CHOICES, default='pending')
    interest_period = models.CharField(max_length=30, choices=P_CHOICES)
    payer = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    receiver = models.ForeignKey(Shares, on_delete=models.CASCADE, null=True, blank=True)
    screenshot = models.FileField(null=True, blank=True)
    date_created = models.DateField(auto_now_add=True)
    last_updated =models.DateField(auto_now=True)
    slug = models.SlugField(null=True, blank=True)
    dispute_status = models.BooleanField(default=False)

    



    def __str__(self):
        return '%s, %s, %s, %s, %s, %s' % (self.trans_id, self.payer, self.amount, self.interest_period, self.receiver.user, self.receiver)


    def save(self, *args, **kwargs):
        if self.trans_id == "":
            ref_code = generate_code()
            code = ref_code
            self.trans_id = code
            self.slug = code

        super().save(*args, **kwargs)



class Volume(models.Model):

    amount = models.IntegerField(default=0, null=True, blank=True)
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return '%s, %s' % (self.amount, self.date_created)



class AdminNotification(models.Model):

    title = models.CharField(max_length=100)
    description = models.TextField()
    date_created = models.DateField(auto_now_add=True)

    def __str__(self):
        return '%s, %s' % (self.title, self.date_created)


class Dispute(models.Model):

    S_CHOICES = [
            ('pending', 'pending'),
            ('awarded', 'awarded')
    ]

    trans = models.OneToOneField(Transaction, on_delete=models.CASCADE)
    payer_screenshot = models.ImageField(upload_to='uploads/', blank=True, null=True)
    receiver_screenshot = models.ImageField(upload_to='uploads/', blank=True, null=True)
    payer_text = models.CharField(max_length=300, blank=True, null=True)
    receiver_text = models.CharField(max_length=300, blank=True, null=True)
    moderator_text = models.CharField(max_length=300, blank=True, null=True)
    status = models.CharField(max_length=10, choices=S_CHOICES, default='pending')


    def __str__(self):
        return '%s' % (self.trans)

