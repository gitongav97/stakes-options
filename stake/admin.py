from django.contrib import admin
from .models import Profile, Shares, Transaction, Volume, AdminNotification, Dispute

# Register your models here.

admin.site.register(Profile)
admin.site.register(Shares)
admin.site.register(Transaction)
admin.site.register(Volume)
admin.site.register(AdminNotification)
admin.site.register(Dispute)

