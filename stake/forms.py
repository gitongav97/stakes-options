from django import forms
from django.forms import ModelForm
from .models import Profile, Transaction, Dispute
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm


User = get_user_model()

P_CHOICES = [
            ('4 days(30%)', '4 days(30%)'),
            ('7 days(50%)', '7 days(50%)'),
]

class NewUserForm(UserCreationForm):
	
	password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	class Meta:
		model = User
		fields = ("username", "email", "first_name", "last_name")
		widgets = {

			'username': forms.TextInput(attrs={'class': 'form-control'}),
        	'email': forms.EmailInput(attrs={'class': 'form-control'}),
        	'first_name': forms.TextInput(attrs={'class': 'form-control'}),
        	'last_name': forms.TextInput(attrs={'class': 'form-control'}),
			
		},

	def clean_password(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords do not match")
		return password1

	def save(self, commit=True):
		user = super(UserCreationForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		user.set_password(self.cleaned_data['password1'])



		if commit:
			user.save()
		return user


class PhoneNumberForm(ModelForm):
	
	class Meta:
		model = Profile
		fields = ('phone_number',)
		widgets = {

			'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
		},


class UserLoginForm(forms.Form):
	
	query = forms.CharField(label='Username / Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	def clean(self, *args, **kwargs):
		
		query = self.cleaned_data.get('query')
		password = self.cleaned_data.get('password')
		user_qs_final = User.objects.filter(
                    Q(username__iexact=query) |
                    Q(email__iexact=query)
                ).distinct()
		

		if not user_qs_final.exists() and user_qs_final.count != 1:
			raise forms.ValidationError("Invalid credentials - user does not exist")
		
		user_obj = user_qs_final.first()
		
		if not user_obj.check_password(password):
			raise forms.ValidationError("Invalid credentials - user does not exist")
			
		self.cleaned_data["user_obj"] = user_obj
		
		return super(UserLoginForm, self).clean(*args, **kwargs)


class TransactionForm(ModelForm):


    # amount = forms.CharField(label='Amount', widget=forms.NumberInput(attrs={'class': 'form-control'}))
    # password = forms.MultipleChoiceField(label='Select', widget=forms.Select(attrs={'class': 'form-control'}), choices=P_CHOICES)
	
    class Meta:
        model = Transaction
        fields = ('amount', 'interest_period')
        widgets = {

            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
            'interest_period': forms.Select(attrs={'class': 'form-control'})
        }

    def __init__(self, *args, **kwargs):
        super(TransactionForm, self).__init__(*args, **kwargs)
        self.fields['amount'].widget.attrs['min'] = 1000


    def clean_price(self):
        amount = self.cleaned_data['amount']
        if amount < 1000:
            raise forms.ValidationError("Amount cannot be less than 1000")



class PayerDisputeForm(ModelForm):

    class Meta:
        model = Dispute
        fields = ('payer_screenshot','payer_text')
        widgets = {

            'payer_screenshot': forms.ImageField(),
            'payer_text': forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'})),
        },

class ReceiverDisputeForm(ModelForm):

    class Meta:
        model = Dispute
        fields = ('receiver_screenshot','receiver_text')
        widgets = {

            'receiver_screenshot': forms.ImageField(),
            'receiver_text': forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'})),
        },


class ModeratorDisputeForm(ModelForm):

    class Meta:
        model = Dispute
        fields = ('moderator_text',)
        widgets = {

            'moderator_text': forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'})),
        },