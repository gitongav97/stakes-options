# Generated by Django 3.2.4 on 2021-08-10 09:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stake', '0030_transaction_dispute_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='dispute',
            old_name='transaction',
            new_name='trans',
        ),
    ]
