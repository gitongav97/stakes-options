# Generated by Django 3.2.4 on 2021-07-04 09:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stake', '0019_alter_transaction_maturity_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='maturity_date',
        ),
    ]
