# Generated by Django 3.2.4 on 2021-08-10 08:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stake', '0029_dispute_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='dispute_status',
            field=models.BooleanField(default=False),
        ),
    ]
