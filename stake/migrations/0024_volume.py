# Generated by Django 3.2.4 on 2021-07-14 05:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stake', '0023_auto_20210704_0936'),
    ]

    operations = [
        migrations.CreateModel(
            name='Volume',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(blank=True, null=True)),
                ('date_created', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
