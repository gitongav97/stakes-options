from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Sum

import datetime
import random
import time

from .forms import UserLoginForm, NewUserForm, PhoneNumberForm, TransactionForm, PayerDisputeForm, ReceiverDisputeForm, ModeratorDisputeForm
from .models import Profile, Shares, Transaction, Volume, AdminNotification, Dispute

from django.contrib.auth.decorators import user_passes_test

from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from django.core import serializers
import calendar

# Create your views here.


def main_view(request):
    
    
    return render(request, 'home.html')


def signup_view(request):

    form = NewUserForm(request.POST or None)
    form2 = PhoneNumberForm(request.POST or None)
    
    
    if form.is_valid() and form2.is_valid():        

        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        phone_number = form2.cleaned_data.get('phone_number')

        instance = form.save()

        registered_user = User.objects.get(id=instance.id)
        registered_profile = Profile.objects.create(user = registered_user)
        registered_profile.phone_number = phone_number
        registered_profile.save()

        user = authenticate(username=username, password=password)
        login(request, user)
        messages.success(request, 'Account Created Successfully')

        return redirect ('dashboard-view')

    context = {
        'form':form,
        'form2':form2,
    }

    return render(request, 'signup.html', context)


def login_view(request):
    
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        messages.success(request, 'Log In Successfully')
        return redirect('dashboard-view')


    context = {
        "form": form
    }

    return render(request, 'login.html', context)



def logout_view(request):

    logout(request)

    return redirect ("main-view")






@login_required(login_url='login-view')
def dashboard_view(request):
    user = request.user

    admin_notifications = AdminNotification.objects.all()[:5]

    active_shares_sum = Shares.objects.filter(status='maturing', user=user).aggregate(Sum('amount'))
    active_shares_sum = active_shares_sum.get("amount__sum")
    if active_shares_sum is None:
        active_shares_sum = 0
    else:
        active_shares_sum  = active_shares_sum


    
    active_shares_return = Shares.objects.filter(status='maturing', user=user).aggregate(Sum('expected_payout'))
    active_shares_return = active_shares_return.get("expected_payout__sum")
    if active_shares_return is None:
        active_shares_return = 0
    else:
        active_shares_return  = active_shares_return



    all_time_shares = Shares.objects.filter(status='completed', user=user).aggregate(Sum('amount'))
    all_time_shares = all_time_shares.get("amount__sum")
    if all_time_shares is None:
        all_time_shares = 0
    else:
        all_time_shares  = all_time_shares



    all_time_returns = Shares.objects.filter(status='completed', user=user).aggregate(Sum('received_payment'))
    all_time_returns = all_time_returns.get("received_payment__sum")
    if all_time_returns is None:
        all_time_returns = 0
    else:
        all_time_returns  = all_time_returns



    try:
        available_shares = Shares.objects.filter(status='matured').exclude(user=user).aggregate(Sum('expected_payout'))
        available_shares_sum = available_shares.get("expected_payout__sum")
    except:
        available_shares_sum = 0
        

    try:
        paid_shares = Shares.objects.filter(status='matured').exclude(user=user).aggregate(Sum('received_payment'))
        paid_shares_sum = paid_shares.get("received_payment__sum")
    except:
        paid_shares_sum = 0
    
        
    total_available_shares = 0
    try:
        total_available_shares = available_shares_sum - paid_shares_sum
    except:
        total_available_shares= 0

    

    context = {
        'active_shares_sum': active_shares_sum,
        'active_shares_return':active_shares_return,
        'all_time_shares':all_time_shares,
        'all_time_returns':all_time_returns,
        'total_available_shares':total_available_shares,
        'admin_notifications':admin_notifications
        
        

    }

    return render(request, 'dashboard.html', context)






@login_required(login_url='login-view')
def buy_stake_view(request):

    user = request.user
    last_updated = datetime.datetime.now()
    today = datetime.datetime.now().date()
    yesterday = datetime.datetime.now().date() - datetime.timedelta(days=1)


    # page data

    try:
        available_shares = Shares.objects.filter(status='matured').exclude(user=user).aggregate(Sum('expected_payout'))
        available_shares_sum = available_shares.get("expected_payout__sum")
    except:
        available_shares_sum = 0
        

    try:
        paid_shares = Shares.objects.filter(status='matured').exclude(user=user).aggregate(Sum('received_payment'))
        paid_shares_sum = paid_shares.get("received_payment__sum")
    except:
        paid_shares_sum = 0
    
        
    total_available_shares = 0
    try:
        total_available_shares = available_shares_sum - paid_shares_sum
    except:
        total_available_shares= 0


    try:
        available_shares_maturing = Shares.objects.filter(status='matured').aggregate(Sum('expected_payout'))
        available_shares_maturing_sum = available_shares_maturing.get("expected_payout__sum")
    except:
        available_shares_maturing_sum = 0

    try:
        maturing_shares = Shares.objects.filter(status='maturing').aggregate(Sum('expected_payout'))
        maturing_shares_sum = maturing_shares.get("expected_payout__sum")
    except:
        maturing_shares_sum = 0


    market_cap = 0
    try:
        if maturing_shares_sum == 0:
            market_cap = available_shares_maturing_sum
        elif available_shares_maturing_sum ==0:
            market_cap = maturing_shares_sum
        else:
            market_cap = available_shares_maturing_sum + maturing_shares_sum
    except:
        market_cap = 0


    try:
        seven_day_data = Volume.objects.all().order_by('-date_created')[:7].aggregate(Sum('amount'))
        seven_day_data_sum = seven_day_data.get("amount__sum")
    except:
        seven_day_data_sum = 0


    try:
        today_data = Volume.objects.filter(date_created=today).aggregate(Sum('amount'))
        today_data_sum = today_data.get("amount__sum")
    except:
        today_data_sum
    
    try:
        yesterday_data = Volume.objects.filter(date_created=yesterday).aggregate(Sum('amount'))
        yesterday_data_sum = yesterday_data.get("amount__sum")
    except:
        yesterday_data_sum = 0


    percent_change = 0
    try:
        percent_change = ((today_data_sum - yesterday_data_sum)/yesterday_data_sum)*100
    except:
        percent_change = 'N/A'

    # end page data



    form = TransactionForm(request.POST or None)    
    if form.is_valid():

        try:
        
            amount = form.cleaned_data.get('amount')
            amount_less = 0
            period= form.cleaned_data.get('interest_period')
            user = request.user

            shares = Shares.objects.filter(status='matured', active_trade=True).exclude(user=user)

            list_of_shares_more=[]
            list_of_shares_less=[]
            for share in shares:

                expected_payout = share.expected_payout
                received_payment = share.received_payment
                balance = expected_payout - received_payment

                if balance >= amount:

                    list_of_shares_more.append(share)

                elif balance < amount:

                    list_of_shares_less.append(share)


            transaction_receiver_more = None
            transaction_receiver_less = None
            try:
                transaction_receiver_more = random.choice(list_of_shares_more)
            except:
                transaction_receiver_less = random.choice(list_of_shares_less)

            
            if transaction_receiver_more is None:
                instance = Transaction.objects.create(payer=user, interest_period=period, amount=transaction_receiver_less.expected_payout-transaction_receiver_less.received_payment, receiver=transaction_receiver_less)
                instance.save()
                messages.info(request, 'The shares amount you requested is not available. The amount has been re-ajusted to the availble amount')
                transaction_receiver_less.active_trade = False
                transaction_receiver_less.save()


                obj = Transaction.objects.get(id=instance.id)
                return redirect ("confirm-buy-stake-view", slug=obj.slug)
            
            
            else:

                instance = Transaction.objects.create(payer=user, interest_period=period, amount=amount, receiver=transaction_receiver_more)
                instance.save()
            
                transaction_receiver_more.active_trade = False
                transaction_receiver_more.save()


                obj = Transaction.objects.get(id=instance.id)

                return redirect ("confirm-buy-stake-view", slug=obj.slug)
            
        
        except:

            messages.info(request, 'The shares you requested are not available. Try again later')

            return redirect('buy-stake-view')
        
    
    context = {
        'form':form,
        'total_available_shares':total_available_shares,
        'market_cap':market_cap,
        'last_updated':last_updated,
        'seven_day_data_sum':seven_day_data_sum,
        'today_data_sum':today_data_sum,
        'yesterday_data_sum':yesterday_data_sum,
        'percent_change':percent_change

    }

    return render(request, 'buy.html', context)






@login_required(login_url='login-view')
def confirm_buy_stake_view(request, slug, *args, **kwargs):
    user = request.user


    trans_details = Transaction.objects.get(slug=slug)
    
    receiver_id = trans_details.receiver.user.id
    payer_profile = Profile.objects.get(user=trans_details.payer.id)

    receiver_profile = Profile.objects.get(user=receiver_id)
    receiver_details = User.objects.get(id=receiver_id)
    names = receiver_details.get_full_name()

    

    

    context = {
        'trans_details':trans_details,
        'receiver_profile':receiver_profile,
        'names':names,
        'receiver_details':receiver_details,
        'payer_profile':payer_profile

    }

    return render(request, 'complete_buy.html', context)
    





@login_required(login_url='login-view')
def confirm_paid_view(request, slug):

    user = request.user
    trans_details = Transaction.objects.get(slug=slug)
    trans_details.status = 'paid'
    trans_details.save()

    return redirect ("confirm-buy-stake-view", slug=trans_details.slug)





@login_required(login_url='login-view')
def approve_paid_view(request, slug):

    user = request.user
    trans_details = Transaction.objects.get(slug=slug)
    trans_details.status = 'approved'
    trans_details.save()
    today = datetime.datetime.now().date()


    

    share_update = Shares.objects.get(id=trans_details.receiver.id)
    paid = share_update.received_payment
    new_paid = trans_details.amount +  paid

    if new_paid == share_update.expected_payout:
        share_update.status = 'completed'
        share_update.received_payment = new_paid
        share_update.save()

    elif new_paid < share_update.expected_payout:
        share_update.received_payment = new_paid
        share_update.active_trade = True
        share_update.save()


    share_user = trans_details.payer
    share_amount = trans_details.amount
    user_interest_period = trans_details.interest_period
    share_status = 'maturing'

    if user_interest_period == '4 days(30%)':
        share_expected_payout = share_amount * 1.3
        share_maturity_date = datetime.datetime.now().date() + datetime.timedelta(days=4)

        instance = Shares.objects.create(user=share_user, amount=share_amount, interest_period=user_interest_period, status=share_status, expected_payout=share_expected_payout, maturity_date=share_maturity_date)

        try:
            vol_update = Volume.objects.get(date_created=today)
            new_vol = vol_update.amount + share_amount
            vol_update.save()
        except:
            Volume.objects.create(amount=share_amount)

    elif user_interest_period == '7 days(50%)':
        share_expected_payout = share_amount * 1.5
        share_maturity_date = datetime.datetime.now() + datetime.timedelta(days=7)

        instance = Shares.objects.create(user=share_user, amount=share_amount, interest_period=user_interest_period, status=share_status, expected_payout=share_expected_payout, maturity_date=share_maturity_date)

        try:
            vol_update = Volume.objects.get(date_created=today)
            new_vol = vol_update.amount + share_amount
            vol_update.save()
        except:
            Volume.objects.create(amount=share_amount)
    

    return redirect ("confirm-buy-stake-view", slug=trans_details.slug)


@login_required(login_url='login-view')
def cancel_trade_view(request, slug):

    user = request.user
    trans_details = Transaction.objects.get(slug=slug)
    trans_details.status = 'cancelled'
    trans_details.save()
    

    share_update = Shares.objects.get(id=trans_details.receiver.id)
    share_update.status = 'matured'
    share_update.active_trade = True
    share_update.save()

    return redirect ("confirm-buy-stake-view", slug=trans_details.slug)


@login_required(login_url='login-view')
def shares_view(request):
    user = request.user

    shares = Shares.objects.filter(user=user)
    
    item_list = []
    for share in shares:

        amount =share.amount
        period = share.interest_period
        payout = share.expected_payout
        state = share.status
        received = share.received_payment
        balance = payout-received
        date = share.date_created
        maturity_date = share.maturity_date

        item = (amount, period, payout, state, received, balance, date, maturity_date)

        item_list.append(item)
    
    context = {
        'item_list':item_list,

    }

    return render(request, 'shares.html', context)





@login_required(login_url='login-view')
def transactions_view(request):

    user = request.user

    trades = Transaction.objects.filter(payer=user).order_by('date_created')
    trades2 = Transaction.objects.filter(receiver__user=user).order_by('date_created')

    context = {
        'trades':trades,
        'trades2':trades2

    }    

    return render(request, 'trades.html', context)





@login_required(login_url='login-view')
def nofifications_view(request):

    admin_notifications = AdminNotification.objects.all()[:5]


    context = {
        'admin_notifications':admin_notifications
    }    

    return render(request, 'notifications.html', context)




@login_required(login_url='login-view')
def support_view(request):

    user = request.user

    disputes = Dispute.objects.all()
    


    context = {
        'disputes':disputes

    }    

    return render(request, 'support.html', context)



@login_required(login_url='login-view')
def raise_dispute_view(request, slug):

    user = request.user

    trans_details = Transaction.objects.get(slug=slug, dispute_status=False)
    dis_pute = Dispute.objects.create(trans=trans_details)
    dis_pute_id = dis_pute.id
    trans_details.dispute_status = True
    trans_details.save()

    
    return redirect('dispute-view', dis_pute_id)

    



@login_required(login_url='login-view')
def dispute_view(request, dis_pute_id):

    user = request.user
    disputed = Dispute.objects.get(id=dis_pute_id)
    payer= disputed.trans.payer
    receiver= disputed.trans.receiver.user
    context = {}

    if user == payer:
        if request.method =='POST':
            payer_form = PayerDisputeForm(request.POST, request.FILES, instance=disputed)
            form = ModeratorDisputeForm(request.POST)
            
            if payer_form.is_valid():
                
                payer_form.save()

                return redirect('dispute-view', dis_pute_id=disputed.id)

            elif form.is_valid():
                disputed.moderator_text = form.cleaned_data.get('moderator_text')
                disputed.status = 'awarded'
                disputed.save()

                return redirect('dispute-view', dis_pute_id=disputed.id)
        else:

            payer_form = PayerDisputeForm()
            form = ModeratorDisputeForm()
        
        context = {
            'disputed':disputed,
            'payer_form':payer_form,
            'payer':payer,
            'form':form
        }
        


    elif user == receiver:
        if request.method =='POST':
            receiver_form = ReceiverDisputeForm(request.POST,request.FILES)
            form = ModeratorDisputeForm(request.POST)
            
            if receiver_form.is_valid():
                disputed.receiver_text = receiver_form.cleaned_data.get('payer_text')
                disputed.receiver_screenshot = receiver_form.receiver_screenshot
                disputed.save()

                return redirect('dispute-view', dis_pute_id=disputed.id)
            
            elif form.is_valid():
                disputed.moderator_text = form.cleaned_data.get('moderator_text')
                disputed.status = 'awarded'
                disputed.save()

                return redirect('dispute-view', dis_pute_id=disputed.id)
        
        else:
            receiver_form = ReceiverDisputeForm()
            form = ModeratorDisputeForm()
        
        context = {

            'disputed':disputed,
            'receiver_form':receiver_form,
            'receiver':receiver,
            'form':form,
        }


    return render(request, 'dispute.html', context)



@login_required(login_url='login-view')
@user_passes_test(lambda u: u.is_superuser)
def users_view(request):

    # staff = User.objects.filter(is_staff=True)
    staff_shares_matured = []
    shares_maturing = []
    staff_transactions_paid = []
    staff_transactions_pending = []
    user_paid = []
    user_pending = []
    #for staff in staff:
        
    shares = Shares.objects.filter(user__is_staff=True, status='matured')
    for share in shares:
        share_id = share.id
        user = share.user
        status = share.status
        expected_payout= share.expected_payout
        received_payment = share.received_payment
        active_trade = share.active_trade
        share_data = (user, status, active_trade, expected_payout, received_payment, share_id)

        staff_shares_matured.append(share_data)


    shares2 = Shares.objects.filter(user__is_staff=True, status='maturing')
    for share in shares2:
        share_id = share.id
        user = share.user
        status = share.status
        expected_payout= share.expected_payout
        received_payment = share.received_payment
        active_trade = share.active_trade
        share_data2 = (user, status, active_trade, expected_payout, received_payment, share_id)

        shares_maturing.append(share_data2)
    


    transaction = Transaction.objects.filter(receiver__user__is_staff=True, status=('paid'))
    for trans in transaction:
        transid = trans.trans_id
        amount = trans.amount
        status = trans.status
        payer = trans.payer
        receiver = trans.receiver.user
        slug= trans.slug
        trans_data = (transid, status, amount, payer, receiver, slug)

    
        staff_transactions_paid.append(trans_data)

    transaction1 = Transaction.objects.filter(receiver__user__is_staff=True, status=('pending'))
    for trans in transaction1:
        transid = trans.trans_id
        amount = trans.amount
        status = trans.status
        payer = trans.payer
        receiver = trans.receiver.user
        slug= trans.slug
        trans_data1 = (transid, status, amount, payer, receiver, slug)

        staff_transactions_pending.append(trans_data1)



    transaction3 = Transaction.objects.filter(receiver__user__is_staff=False, status=('paid'))
    for trans in transaction3:
        transid = trans.trans_id
        amount = trans.amount
        status = trans.status
        payer = trans.payer
        receiver = trans.receiver.user
        slug= trans.slug
        trans_data3 = (transid, status, amount, payer, receiver, slug)

    
        user_paid.append(trans_data3)
        

    transaction4 = Transaction.objects.filter(receiver__user__is_staff=False, status=('pending'))
    for trans in transaction4:
        transid = trans.trans_id
        amount = trans.amount
        status = trans.status
        payer = trans.payer
        receiver = trans.receiver.user
        slug= trans.slug
        trans_data4 = (transid, status, amount, payer, receiver, slug)

        user_pending.append(trans_data4)


    all_disputes = Dispute.objects.filter(status='pending')


    

    context = {
        'staff_shares_matured':staff_shares_matured,
        'shares_maturing':shares_maturing,
        'staff_transactions_paid':staff_transactions_paid,
        'staff_transactions_pending':staff_transactions_pending,
        'user_paid':user_paid,
        'user_pending':user_pending,
        'all_disputes':all_disputes

    }    

    return render(request, 'users.html', context)



@login_required(login_url='login-view')
def shares_update(request):

    matured_shares = Shares.objects.filter(status='maturing', user__is_staff=False)
    for share in matured_shares:
        today = datetime.datetime.now().date() 
        share_maturity_date = share.maturity_date
        share_maturity_date = share_maturity_date.date()

        if today >= share_maturity_date:
            share.status ='matured'
            share.save()

    print('updating...')

    return redirect ('users-view')


@login_required(login_url='login-view')
def staff_shares_update(request):

    matured_shares = Shares.objects.filter(status='maturing', user__is_staff=True)
    for share in matured_shares:
        today = datetime.datetime.now().date() 
        share_maturity_date = share.maturity_date
        share_maturity_date = share_maturity_date.date()

        if today >= share_maturity_date:
            share.status ='matured'
            share.save()

    print('updating...')

    return redirect ('users-view')


@login_required(login_url='login-view')
def deactivate_share_trade(request, shared):

    share_update = Shares.objects.get(id=shared)
    share_update.active_trade = False
    share_update.save()

    return redirect ('users-view')


@login_required(login_url='login-view')
def activate_share_trade(request, shared):

    share_update = Shares.objects.get(id=shared)
    share_update.active_trade = True
    share_update.save()

    return redirect ('users-view')



@login_required(login_url='login-view')
def deactivate_staff_shares(request):

    share_update = Shares.objects.filter(user__is_staff=True, status='matured')
    for share in share_update:
        share.active_trade = False
        share.save()

    return redirect ('users-view')


@login_required(login_url='login-view')
def activate_staff_shares(request):

    share_update = Shares.objects.filter(user__is_staff=True, status='matured')
    for share in share_update:
        share.active_trade = True
        share.save()

    return redirect ('users-view')



@login_required(login_url='login-view')
def deactivate_user_shares(request):

    share_update = Shares.objects.filter(user__is_staff=False, status='matured')
    for share in share_update:
        share.active_trade = False
        share.save()

    return redirect ('users-view')


@login_required(login_url='login-view')
def activate_user_shares(request):

    share_update = Shares.objects.filter(user__is_staff=False, status='matured')
    for share in share_update:
        share.active_trade = True
        share.save()

    return redirect ('users-view')



class ChartData(APIView):

    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        
        volume_amount_chart = []
        date_chart = []
        profit_chart = []
    
        volume_data = Volume.objects.all().order_by('-date_created')[:10]
        volume_data = reversed(volume_data)
        

        for data in volume_data:

            amount = data.amount
            date = data.date_created
            date = date.strftime("%b %d,  %Y ")
            profit = int(data.amount)*0.4

            volume_amount_chart.append(amount)
            date_chart.append(date)
            profit_chart.append(profit)


            


        data = {
            'volume_amount_chart':volume_amount_chart,
            'date_chart':date_chart,
            'profit_chart':profit_chart
        }

        return JsonResponse(data)