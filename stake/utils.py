import uuid


def generate_code():
    ref_code = str(uuid.uuid4())[:12]
    return ref_code